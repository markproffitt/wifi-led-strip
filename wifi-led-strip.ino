/*
   Captive Portal by: M. Ray Burnette 20150831
   See Notes tab for original code references and compile requirements
   Sketch uses 300,640 bytes (69%) of program storage space. Maximum is 434,160 bytes.
   Global variables use 50,732 bytes (61%) of dynamic memory, leaving 31,336 bytes for local variables. Maximum is 81,920 bytes.
*/

#include <ESP8266WiFi.h>
#include "./DNSServer.h"                  // Patched lib
#include <ESP8266WebServer.h>
#include "FastLED.h"

FASTLED_USING_NAMESPACE


#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    5
#define POWER_PIN    14
//#define CLK_PIN   4
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    184
CRGB leds[NUM_LEDS];

// #define BRIGHTNESS          255
short gBrightness = 196;
#define FRAMES_PER_SECOND  120    //120
uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

const byte        DNS_PORT = 53;          // Capture DNS requests on port 53
IPAddress         apIP(10, 10, 10, 1);    // Private network for server
DNSServer         dnsServer;              // Create the DNS object
ESP8266WebServer  webServer(80);          // HTTP server

String gstrModes[] = { "Rainbow", "Fade", "Confetti", "Sinelon", "Juggle", "BPM", 
            "Red", "Green", "Blue", "Yellow", "Violet", "Off" };

String htmlMenu =     "<div class=\"line\">"
                      "<a href=\"http://10.10.10.1/color?mode=0\"><button>Rainbow</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=1\"><button>Fade</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=2\"><button>Confetti</button></a></div>"
                      "<div class=\"line\">"
                      "<a href=\"http://10.10.10.1/color?mode=3\"><button>Sinelon</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=4\"><button>Juggle</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=5\"><button>BPM</button></a></div>"
                      "<div class=\"line\">"
                      "<a href=\"http://10.10.10.1/color?mode=6\"><button>Red</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=7\"><button>Green</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=8\"><button>Blue</button></a></div>"
                      "<div class=\"line\">"
                      "<a href=\"http://10.10.10.1/color?mode=9\"><button>Yellow</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=10\"><button>Violet</button></a>"
                      "<a href=\"http://10.10.10.1/color?mode=11\"><button>Off</button></a></div>";
String htmlHeader =   "<!DOCTYPE html><html><head><title>LED Lyra Menu</title>"
                      "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">"
                      "<style>button{padding:1em;margin:0.5em;}"
                      ".line{width:100%;margin:0.2em;}html{width:100%;}body{width100%;}"
                      "</style>"
                      "</head><body>";

String responseHTML = htmlHeader +
                      "<h2>LED Lyra Menu</h2><p>Select the LED mode.</p>" +htmlMenu+
                      "</body></html>";

long stringToLong(String s)
{
   char arr[12];
   s.toCharArray(arr, sizeof(arr));
   return atol(arr);
}
void handleColor() {
  String strMode = webServer.arg("mode");
  int cmode = stringToLong(strMode);
  String strHTML =  htmlHeader +
                    "<h2>LED Lyra Menu</h2><h3>"+gstrModes[cmode]+"</h3><p>Select the LED mode.</p>" +
                    htmlMenu +
                    "</body></html>";
  gCurrentPatternNumber =  cmode;   // Verify not 
  webServer.send(200, "text/html", strHTML);    
}
void handleOff() {
    String strHTML =  htmlHeader +
                      "<h2>LED Lyra Menu</h2><h3>Off</h3><p>Select the LED mode.</p>" +
                      htmlMenu +
                      "</body></html>";
    gCurrentPatternNumber = 11;
    webServer.send(200, "text/html", strHTML);  
}
void setup() {
  pinMode(POWER_PIN, OUTPUT);
  Serial.begin(9600);  
  delay(3000); // 3 second delay for recovery
  
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  //FastLED.addLeds<LED_TYPE,DATA_PIN,CLK_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

  // set master brightness control
  FastLED.setBrightness(gBrightness);

  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  WiFi.softAP("LED Lyra","12345678");

  // if DNSServer is started with "*" for domain name, it will reply with
  // provided IP to all DNS request
  dnsServer.start(DNS_PORT, "*", apIP);

  webServer.on("/color", handleColor);

  // replay to all requests with same HTML
  webServer.onNotFound([]() {
    webServer.send(200, "text/html", responseHTML);
  });
  webServer.begin();
}


// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm, 
            red, green, blue, yellow, violet, off };

void loop() {
  digitalWrite(POWER_PIN, HIGH);
  dnsServer.processNextRequest();
  webServer.handleClient();
  // Call the current pattern function once, updating the 'leds' array
  gPatterns[gCurrentPatternNumber]();

  // set master brightness control
  FastLED.setBrightness(gBrightness);
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND); 
  Serial.println(gCurrentPatternNumber);

  // do some periodic updates
  EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow
//  EVERY_N_SECONDS( 10 ) { nextPattern(); } // change patterns periodically
}

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}

void rainbow() 
{
  gBrightness = 255;
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter() 
{
  gBrightness = 196;
  // built-in FastLED rainbow, plus some random sparkly glitter
  fadeToBlackBy( leds, NUM_LEDS, 20);
  fill_solid( leds, NUM_LEDS, CHSV( gHue, 255, 192));   
//  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti() 
{
  gBrightness = 255;
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  gBrightness = 255;
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 13, 0, NUM_LEDS-1 );
  leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
  gBrightness = 255;
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 80;  //62
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void juggle() {
  gBrightness = 255;
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    leds[beatsin16( i+7, 0, NUM_LEDS-1 )] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

void red() {
  gBrightness = 128;
/*
  for( int i=0; i<NUM_LEDS; i=i+2) {
    leds[i] = CRGB::Red;
    leds[i+1] = CRGB::Black;
  }
*/  
  fill_solid( leds, NUM_LEDS, CRGB::Red);   
}
void green() {
  gBrightness = 196;
  fill_solid( leds, NUM_LEDS, CRGB::Green);   
}
void blue() {
  gBrightness = 196;
  fill_solid( leds, NUM_LEDS, CRGB::Blue);   
}
void yellow() {
  gBrightness = 128;
/*  
  for( int i=0; i<NUM_LEDS; i=i+2) {
    leds[i] = CRGB::Yellow;
    leds[i+1] = CRGB::Black;
  }
*/  
  fill_solid( leds, NUM_LEDS, CRGB::Yellow);   
}
void violet() {
  gBrightness = 196;
  fill_solid( leds, NUM_LEDS, CRGB::Purple);   
}
void off() {
  gBrightness = 0;
//  fill_solid( leds, NUM_LEDS, CRGB::Black);   
}

